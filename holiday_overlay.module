<?php

/**
 * @file
 *
 * Holiday Overlay module for Drupal.
 */

/**
 * Implements hook_permission().
 */
function holiday_overlay_permission() {
  return array(
    'administer holiday overlay' => array(
      'title' => t('Administer Holiday Overlay'),
      'description' => t('Change Holiday Overlay settings.'),
    ),
    'override holiday overlay' => array(
      'title' => t('Override Holiday Overlay'),
      'description' => t("Don't show the holiday overlay to users with this permission."),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function holiday_overlay_menu() {
  $items['admin/config/system/holiday_overlay'] = array(
    'title' => 'Holiday Overlay',
    'access arguments' => array('administer holiday overlay'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('holiday_overlay_settings_page'),
  );

  return $items;
}

/**
 * Implements hook_page_alter().
 */
function holiday_overlay_page_alter(&$page) {
  // If overlay should be shown, add overlay content, js, and css.
  if (holiday_overlay_is_active()) {
    // Add message to JS settings for overlay.
    $message = variable_get('holiday_overlay_message', array('value' => _holiday_overlay_defaults('message'), 'format' => 1));
    $message = check_markup($message['value'], $message['format']);
    drupal_add_js(array('holidayOverlayMessage' => $message), 'setting');

    // Add overlay JS and CSS.
    drupal_add_js(drupal_get_path('module', 'holiday_overlay') . '/js/holiday_overlay.overlay.js', 'file');
    drupal_add_css(drupal_get_path('module', 'holiday_overlay') . '/css/holiday_overlay.styles.css', array('group' => CSS_DEFAULT, 'every_page' => TRUE));
  }
}

/**
 * Function to determine whether the holiday overlay should be enabled.
 */
function holiday_overlay_is_active() {
  global $user;

  // Return FALSE if the user has override permission.
  if (user_access('override holiday overlay')) {
    return FALSE;
  }

  $current_date = date('n j Y');
  $date = variable_get('holiday_overlay_date', _holiday_overlay_defaults('date'));
  if ($current_date == $date['month'] . ' ' . $date['day'] . ' ' . $date['year']) {
    dpm('MATCH');
    return TRUE;
  }

  return FALSE;
}

/**
 * Page callback for holiday overlay settings.
 */
function holiday_overlay_settings_page() {
  $form['description'] = array(
    '#markup' => '<p>' . t('Change Holiday Overlay settings on this page.') . '</p>',
  );

  // Set default date of Christmas (current year) and define date field.
  $date = variable_get('holiday_overlay_date', _holiday_overlay_defaults('date'));
  $form['holiday_overlay_date'] = array(
    '#type' => 'date',
    '#title' => t('Holiday Overlay Date'),
    '#description' => t('The date during which the Holiday Overlay message will be shown to everyone without override permission.'),
    '#default_value' => $date,
    '#required' => TRUE,
  );

  // Set default message and define message field.
  $message = variable_get('holiday_overlay_message', array('value' => _holiday_overlay_defaults('message'), 'format' => 1));
  $form['holiday_overlay_message'] = array(
    '#type' => 'text_format',
    '#title' => t('Message to Display'),
    '#description' => t('The message that will be shown inside the Holiday Overlay on the date specified above.'),
    '#default_value' => $message['value'],
    '#format' => $message['format'],
  );

  return system_settings_form($form);
}

/**
 * Returns defaults for Holiday Overlay.
 *
 * @param $option
 *   - 'message' - returns the default message (string).
 *   - 'date' - returns the default date (array).
 */
function _holiday_overlay_defaults($option) {
  if ($option == 'message') {
    return t("This website celebrates Christmas. Please enjoy the holiday with your family instead of spending time browsing the web.");
  }
  elseif ($option == 'date') {
    return array(
      'year' => date('Y'),
      'month' => 12,
      'day' => 25,
    );
  }
}
